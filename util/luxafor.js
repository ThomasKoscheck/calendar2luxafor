const { device, devices } = require('luxafor-api');

const luxafor = device(); // use devices() to get all devices

exports.setColor = function setColor(color){
    luxafor.color(color); // or use hex color e.g. #fff
    console.log("Set color to", color)
}

exports.turnOff = function turnOff(){
    luxafor.off();
    console.log("Turned off all leds")
}

