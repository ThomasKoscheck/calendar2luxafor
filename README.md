# Calendar2Luxafor

## Install on Raspberry Pi
### Install node
https://gist.github.com/davps/6c6e0ba59d023a9e3963cea4ad0fb516

### Compile node-hid from sources
```
sudo apt-get install libusb-1.0-0-dev
sudo apt-get install libudev-dev
```

and install as shown here: https://github.com/node-hid/node-hid#compiling-from-source

Note: For me it was not necessary to install *g++-4.8*

## Connect Pi to WLAN

https://www.raspberrypi.org/documentation/configuration/wireless/wireless-cli.md

## Authorize Google Calender API
https://developers.google.com/calendar/quickstart/nodejs

On the first run you have to manually complete the authorization process

## Run script
Start with `nohup sudo node index.js &`