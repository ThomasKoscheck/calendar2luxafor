const calendar = require('./util/calendar/')
const luxafor = require('./util/luxafor')
const schedule = require('node-schedule');
const express = require("express");
const bodyParser = require('body-parser');

//////////////
// REST API for website
//////////////
const app = express();

// settings
app.use(bodyParser.json()); // support json encoded bodies
app.use(bodyParser.urlencoded({ extended: true })); // support encoded bodies

app.post("/api/setColor", (req, res, next) => {
    const color = req.body.color

    luxafor.setColor(color)
    res.send(color);
});

app.post("/api/turnOff", (req, res, next) => {

    luxafor.turnOff()
    res.send("off");
});

app.listen(3000, () => {
    console.log("Server running on port 3000");
});


//////////////
// Calendar Logic
//////////////
let pollIntervallTime = 1000 * 60 * 5 //every 5 minutes
let pollIntervallObject
let nextRedSchedule, nextGreenSchedule, nextEventId

////
// Initialization
////
// start freshly with green
luxafor.setColor('green')

// pull new data on beginning
handleEventData()

// turn of light and slow down polling everyday at midnight
schedule.scheduleJob('0 0 * * *', function(){
    luxafor.turnOff()

    // slow down polling in the night
    pollIntervallTime = 1000 * 60 * 120  // every 2h
    clearInterval(pollIntervallObject)
    polling()

    console.log('Turned of light and I am now in slow polling mode')
});

// turn of light and start polling faster everyday at 6 am
schedule.scheduleJob('0 6 * * *', function(){
    luxafor.setColor('green')

    // reset to fast polling
    pollIntervallTime = 1000 * 60 * 5  // every 5m
    clearInterval(pollIntervallObject)
    polling()

    console.log('Started fast polling at morning')
});

////
// Main
////
// start polling
polling()

///
// Functions
////
function polling() {
    pollIntervallObject = setInterval(function(){
        console.log('Currently polling every', pollIntervallTime, 'milliseconds')
        handleEventData()
    }, pollIntervallTime);
}

function handleEventData() {
    calendar.getUpcomingEvent(function (event) {
        console.log('Next event:')
        console.log(event)
        console.log('---------------------\n')

        // check if we have a new next event
        if (event.id !== nextEventId) {
            console.log('Detected new event,', event.id)
            console.log('Old event id, ', nextEventId)

            // if there is no '//' in the event title is a private event an we will exclude it
            // transparency = transparent is equal to 'Show me as Available' while opaque is equal to "Show me as Busy (default)
            // transparency is not included in event data when it is set to opaque
            if (event.summary.includes('//')) {
                console.log('New event has // in title')
                if (event.transparency !== 'transparent') {
                    console.log('New event is set to "Show as busy"')
                    nextEventId = event.id

                    setNextRed(event.start.dateTime)
                    setNextGreen(event.end.dateTime)
                }
            }
        }
    })
}

function setNextRed(time) {
    const date = new Date(time);

    console.log('Next red time scheduled for', date)
    nextRedSchedule = schedule.scheduleJob(date, function(){
        try {
            // cancel old schedule
            nextRedSchedule.cancel();
        } catch (error) {
            console.log(error)
        }
        finally {
            luxafor.setColor('red')
        }
    });
}

function setNextGreen(time) {
    const date = new Date(time);

    console.log('Next green time scheduled for ', date)
    nextGreenSchedule = schedule.scheduleJob(date, function(){
        try {
            // cancel old schedule
            nextGreenSchedule.cancel();
        } catch (error) {
            console.log(error)
        }
        finally {
            luxafor.setColor('green')
        }
    });
}